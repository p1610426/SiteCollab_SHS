#!/bin/bash

docker-compose up -d
echo "Les sites sont créées."
echo "Installation de wordpress en cours..."
sleep 40
docker run -it --rm --volumes-from wordpress --network container:wordpress wordpress:cli core install --url="http://localhost/wordpress/" --title="$(grep "WORDPRESS_WEBSITE_TITLE" .env | cut -d = -f2)" --admin_user=$(grep "WORDPRESS_ADMIN_USERNAME" .env | cut -d = -f2) --admin_password=$(grep "WORDPRESS_ADMIN_PASSWORD" .env | cut -d = -f2) --admin_email=$(grep "WORDPRESS_ADMIN_MAIL" .env | cut -d = -f2)
sed -i.save -e "s/);/\t'overwritewebroot' => 'owncloud',\n);/g" data_owncloud/config/config.php
echo "Installation terminé."