# SiteCollab_SHS

http://perso.univ-lyon1.fr/fabien.rico/site/projet:2018:aut:sujets
Sujet FR2.


Il faut installer Docker et Docker-compose au préalable.

Les lignes de commandes pour chaque installation :

1) Création du fichier .env qui va contenir les configurations des applications, veuillez modifier les variables avant de copier et entrer la commande :

cat << EOF > .env
OWNCLOUD_ADMIN_USERNAME=admin
OWNCLOUD_ADMIN_PASSWORD=admin

WORDPRESS_ADMIN_USERNAME=admin
WORDPRESS_ADMIN_PASSWORD=admin
WORDPRESS_ADMIN_MAIL=admin@mail.com
WORDPRESS_WEBSITE_TITLE=Site de la mort qui tue

MYSQL_DATABASE=exampledb
MYSQL_USER=exampleuser
MYSQL_PASSWORD=examplepass
MYSQL_ROOT_PASSWORD=titi

MARIADB_ROOT_PASSWORD=owncloud
MARIADB_USERNAME=owncloud
MARIADB_PASSWORD=owncloud
MARIADB_DATABASE=owncloud

ROCKETCHAT_USERNAME=admin
ROCKETCHAT_PASSWORD=admin
ROCKETCHAT_MAIL=admin@email.com

LDAP_ADMIN_PASSWORD=admin
LDAP_CONFIG_PASSWORD=config
LDAP_ORGANISATION=Example Inc.
LDAP_DOMAIN=example.org


EOF


2) Installation des sites et lancement cette commande soit être lancé en mode root :

./install.sh


3) Les sites sont maintenant accessibles via les liens suivant sur le serveur local :	
	
	- http://localhost/wordpress
	- http://localhost/owncloud
	- http://localhost/rocketchat

4) La configuration de OpenLDAP :
	- aller sur le navigateur et mettez dans l'url : localhost:6080
	- connectez-vous en utilisant le mot de passe admin que vous aviez entré dans le .env
	- Cliquez sur "Create a new entry here", cochez "Generic : posix group". Entrez le nom du groupe et validez puis commit (ex: users).
	- Cliquez sur "Create a new entry here", cochez "Generic : Organisational unit". Entrer le nom du groupe d'utilisateur (ex :people).
	*
	- Cliquez sur votre groupe comment par "ou=", et cliquez sur "Create a new entry here" dans la partie de droite, et choisissez "Generic : User Account". Entrez les informations concernant l'utilisateur. Choisissez dans le "GID Number" le groupe posix que vous aviez créé précedemment.
	- Cliquez sur le compte que vous aviez créé precedemment, et cliquez sur "Add new attribute", choisissez email et entrer le mail de l'utilisateur.
	*
	- Pour créer d'autres utilisateurs, répétez l'étape entre "*".


5a) Pour configurer le LDAP sur rocketchat :
	- Cliquez sur les 3 petits points et cliquez sur "Administration".
	- Dans la barre de recherche tapez "LDAP" et cliquez dessus.
	- Commencez par cochez True dans le champ "enable". Dans le champ "Host" entrez "openldap", dans le champ "port" entrez "389" et dans "Base DN" entrez la base de votre LDAP.
	- Dans Authentification, cochez "True" dans Enable, puis entre les identifiant admin de LDAP.
	- Dans sync/Import, remplacez le contenu de "Username field" par "uid" et ajoutez "uid" dans le champ "Unique Identifier Field".
	- Dans User search, cherchez le champ "Search Field" et remplacez le contenu par "uid".
	- Cliquez sur "Save Change" et testez la connexion.

5b) Pour configuration de LDAP sur les deux autres services, Il faut installer les plugins et suivre la documentation suivante :
	- owncloud : https://doc.owncloud.org/server/10.0/admin_manual/configuration/user/user_auth_ldap.html
	- wordpress : https://fr.wordpress.org/plugins/ldap-login-for-intranet-sites/#installation


